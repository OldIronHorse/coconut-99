from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='coconut-99',
      version='0.0.1',
      description='99 Problems in Coconut',
      long_description=readme(),
      long_description_content_type='text/markdown',
      keywords='',
      url='http://gitlab.com/OldIronHorse/coconut-99',
      author='Simon Redding',
      author_email='s1m0n.r3dd1ng@gmail.com',
      license='GPL3',
      packages=['coconut99'],
      scripts=[],
      python_requires='>=3.6',
      install_requires=[
          'coconut',
          ],
      test_suite='nose.collector',
      tests_require=['nose', 'nosy'],
      include_package_data=True,
      zip_safe=False)
